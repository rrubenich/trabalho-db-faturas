/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.modelo;

import java.sql.Date;

/**
 *
 * @author rafakx
 */
public class NotaFiscal {
    private int idNotaFiscal;
    private Date dataEmissao;
    private int idCliente;
    private int idVendedor;

    public NotaFiscal(int idNotaFiscal, Date dataEmissao, int idCliente, int idVendedor) {
        this.idNotaFiscal = idNotaFiscal;
        this.dataEmissao = dataEmissao;
        this.idCliente = idCliente;
        this.idVendedor = idVendedor;
    }

    public int getIdNotaFiscal() {
        return idNotaFiscal;
    }

    public void setIdNotaFiscal(int idNotaFiscal) {
        this.idNotaFiscal = idNotaFiscal;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }
}
