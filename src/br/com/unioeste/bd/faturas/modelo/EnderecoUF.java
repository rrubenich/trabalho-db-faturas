/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.modelo;

/**
 *
 * @author rafakx
 */
public class EnderecoUF {
    private int idUF;
    private String nomeUF;

    public EnderecoUF(int idUF, String nomeUF) {
        this.idUF = idUF;
        this.nomeUF = nomeUF;
    }
    
    public int getIdUF() {
        return idUF;
    }

    public void setIdUF(int idUF) {
        this.idUF = idUF;
    }
    
    public String getNomeUF() {
        return nomeUF;
    }

    public void setNomeUF(String nomeUF) {
        this.nomeUF = nomeUF;
    }

    @Override
    public String toString() {
        return nomeUF;
    }

}
