/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.modelo;

import java.sql.Date;

/**
 *
 * @author rafakx
 */
public class Fatura {
    private int idFatura;
    private Date dataEmissao;
    private Date dataVencimento;
    private int valorFatura;
    private int idSituacao;
    private int idNotaFiscal;

    public Fatura(int idFatura, Date dataEmissao, Date dataVencimento, int valorFatura, int idSituacao, int idNotaFiscal) {
        this.idFatura = idFatura;
        this.dataEmissao = dataEmissao;
        this.dataVencimento = dataVencimento;
        this.valorFatura = valorFatura;
        this.idSituacao = idSituacao;
        this.idNotaFiscal = idNotaFiscal;
    }

    public int getIdFatura() {
        return idFatura;
    }

    public void setIdFatura(int idFatura) {
        this.idFatura = idFatura;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public int getValorFatura() {
        return valorFatura;
    }

    public void setValorFatura(int valorFatura) {
        this.valorFatura = valorFatura;
    }

    public int getIdSituacao() {
        return idSituacao;
    }

    public void setIdSituacao(int idSituacao) {
        this.idSituacao = idSituacao;
    }

    public int getIdNotaFiscal() {
        return idNotaFiscal;
    }

    public void setIdNotaFiscal(int idNotaFiscal) {
        this.idNotaFiscal = idNotaFiscal;
    }
}
