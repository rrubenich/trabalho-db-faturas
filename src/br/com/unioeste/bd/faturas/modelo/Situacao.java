/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.modelo;

/**
 *
 * @author rafakx
 */
public class Situacao {
    private int idSituacao;
    private String nomeSituacao;

    public Situacao(int idSituacao, String nomeSituacao) {
        this.idSituacao = idSituacao;
        this.nomeSituacao = nomeSituacao;
    }

    public int getIdSituacao() {
        return idSituacao;
    }

    public void setIdSituacao(int idSituacao) {
        this.idSituacao = idSituacao;
    }

    public String getNomeSituacao() {
        return nomeSituacao;
    }

    public void setNomeSituacao(String nomeSituacao) {
        this.nomeSituacao = nomeSituacao;
    }
    @Override
    public String toString() {
        return nomeSituacao;
    }
}
