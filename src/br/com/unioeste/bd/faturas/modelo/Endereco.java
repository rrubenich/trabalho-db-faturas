/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.modelo;

/**
 *
 * @author rafakx
 */
public class Endereco {
    private int idEndereco;
    private String cep;
    private int idRua;
    private int idBairro;
    private int idCidade;
    
    public Endereco(int idEndereco, String cep, int idRua, int idBairro, int idCidade) {
        this.idEndereco = idEndereco;
        this.cep = cep;
        this.idRua = idRua;
        this.idBairro = idBairro;
        this.idCidade = idCidade;
    }
    
    public int getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public int getIdRua() {
        return idRua;
    }

    public void setIdRua(int idRua) {
        this.idRua = idRua;
    }

    public int getIdBairro() {
        return idBairro;
    }

    public void setIdBairro(int idBairro) {
        this.idBairro = idBairro;
    }

    public int getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(int idCidade) {
        this.idCidade = idCidade;
    }

   
}
