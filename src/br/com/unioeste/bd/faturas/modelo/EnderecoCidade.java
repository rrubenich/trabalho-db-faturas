/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.modelo;

/**
 *
 * @author rafakx
 */
public class EnderecoCidade {
    private int idCidade;
    private String nomeCidade;
    private int idUF;

    public EnderecoCidade(int idCidade, String nomeCidade, int idUF) {
        this.idCidade = idCidade;
        this.nomeCidade = nomeCidade;
        this.idUF = idUF;
    }

    public int getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(int idCidade) {
        this.idCidade = idCidade;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public int getIdUF() {
        return idUF;
    }

    public void setIdUF(int idUF) {
        this.idUF = idUF;
    }
}
