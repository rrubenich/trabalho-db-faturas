/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.modelo;

/**
 *
 * @author rafakx
 */
public class EnderecoRua {
    private int idRua;
    private String nomeRua;

    public EnderecoRua(int idRua, String nomeRua) {
        this.idRua = idRua;
        this.nomeRua = nomeRua;
    }

    public int getIdRua() {
        return idRua;
    }

    public void setIdRua(int idRua) {
        this.idRua = idRua;
    }

    public String getNomeRua() {
        return nomeRua;
    }

    public void setNomeRua(String nomeRua) {
        this.nomeRua = nomeRua;
    }
}
