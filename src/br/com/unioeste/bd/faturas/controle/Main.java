/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;


import br.com.unioeste.bd.faturas.visao.VisaoFatura;
import br.com.unioeste.bd.faturas.visao.VisaoNotaFiscal;
import br.com.unioeste.bd.faturas.visao.VisaoVendedor;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
/**
 *
 * @author rafakx
 */
public class Main {
    public static void main(String[] args) {
        try{  
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()){  
                if ("GTK+".equals( info.getName())){  
                    UIManager.setLookAndFeel( info.getClassName() );  
                    break;  
                }  
            }  
        }
        catch(UnsupportedLookAndFeelException exc){  
            exc.printStackTrace();  
        }
        catch(ClassNotFoundException exc) {  
            exc.printStackTrace();  
        }
        catch(InstantiationException exc) {  
            exc.printStackTrace();  
        }
        catch(IllegalAccessException exc) {  
            exc.printStackTrace();  
        }
        
        new VisaoFatura().setVisible(true);
    }
    
}
