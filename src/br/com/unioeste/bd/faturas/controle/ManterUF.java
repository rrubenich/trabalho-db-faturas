/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.EnderecoUF;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterUF {

    Conexao c = new Conexao();
    
    public boolean Create(EnderecoUF uf){
        
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO enderecoUF VALUES(null,?)");
            c.declaracao.setString(1, uf.getNomeUF());
            
            c.declaracao.execute();

            c.fechaBD();
            return true;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return false;
        }
    }

    public EnderecoUF Read(int idUF){
        
        try {
            
            String nomeUF = null;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoUF WHERE idUF = ?");
            c.declaracao.setInt(1, idUF);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               nomeUF = c.resultado.getString("nomeUF");
            }
            
            EnderecoUF uf = new EnderecoUF(idUF, nomeUF);
        
            
            c.fechaBD();
            return uf;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idUF){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM enderecoUF WHERE idUF = ?");
            c.declaracao.setInt(1, idUF);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<EnderecoUF> List(){
        ArrayList<EnderecoUF> ufs = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idUF;
            String nomeUF;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoUF");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idUF   = c.resultado.getInt("idUF");
               nomeUF = c.resultado.getString("nomeUF");
                
                
               EnderecoUF uf = new EnderecoUF(idUF, nomeUF);
        
            
               ufs.add(uf);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return ufs;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }

}
