/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.Situacao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterSituacao {
    
    Conexao c = new Conexao();
    
    public boolean Create(Situacao sit){
        
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO situacao VALUES(null,?)");
            c.declaracao.setString(1, sit.getNomeSituacao());
            
            c.declaracao.execute();

            c.fechaBD();
            return true;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return false;
        }
    }

    public Situacao Read(int idSituacao){
        
        try {
            
            String nomeSituacao = null;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM situacao WHERE idSituacao = ?");
            c.declaracao.setInt(1, idSituacao);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               nomeSituacao = c.resultado.getString("nomeSituacao");
            }
            
            Situacao sit = new Situacao(idSituacao, nomeSituacao);
        
            
            c.fechaBD();
            return sit;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idSituacao){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM situacao WHERE idSituacao = ?");
            c.declaracao.setInt(1, idSituacao);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<Situacao> List(){
        ArrayList<Situacao> situacoes = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idSituacao;
            String nomeSituacao;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM situacao");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idSituacao   = c.resultado.getInt("idSituacao");
               nomeSituacao = c.resultado.getString("nomeSituacao");
                
                
               Situacao sit = new Situacao(idSituacao, nomeSituacao);
        
            
               situacoes.add(sit);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return situacoes;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
}
