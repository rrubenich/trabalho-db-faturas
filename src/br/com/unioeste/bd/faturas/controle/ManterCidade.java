/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.EnderecoCidade;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterCidade {

    Conexao c = new Conexao();
    
    public int Create(EnderecoCidade cid){
        int ret = 0;
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO enderecoCidade VALUES(null,?,?)", Statement.RETURN_GENERATED_KEYS);
            c.declaracao.setString(1, cid.getNomeCidade());
            c.declaracao.setInt(2, cid.getIdUF());
            
            c.declaracao.execute();

            try (ResultSet keys = c.declaracao.getGeneratedKeys()) {

                keys.next();
                ret = keys.getInt(1);
                keys.close();
            }
            
            c.fechaBD();
            
            return ret;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return ret;
        } 
    }
    
    
    public EnderecoCidade Read(int idCidade){
        
        try {
            
            String nomeCidade = null;
            int idUF = 0;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoCidade WHERE idCidade = ?");
            c.declaracao.setInt(1, idCidade);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               nomeCidade = c.resultado.getString("nomeCidade");
            }
            
            EnderecoCidade cid = new EnderecoCidade(idCidade, nomeCidade, idUF);
        
            
            c.fechaBD();
            return cid;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idCidade){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM enderecoCidade WHERE idCidade = ?");
            c.declaracao.setInt(1, idCidade);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<EnderecoCidade> List(){
        ArrayList<EnderecoCidade> cidades = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idCidade;
            int idUF;
            String nomeCidade;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoCidade");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idCidade   = c.resultado.getInt("idCidade");
               idUF       = c.resultado.getInt("idUF");
               nomeCidade = c.resultado.getString("nomeCidade");
                
                
               EnderecoCidade cid = new EnderecoCidade(idCidade, nomeCidade, idUF);
        
            
               cidades.add(cid);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return cidades;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }

    public ArrayList<EnderecoCidade> Search(String query){
        ArrayList<EnderecoCidade> cidades = new ArrayList<>();
        
        try{
            c.abreBD();
            
             int idCidade;
            int idUF;
            String nomeCidade;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoCidade WHERE nomeCidade LIKE ?");
            c.declaracao.setString(1, "%" + query + "%");
            c.resultado = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idCidade   = c.resultado.getInt("idCidade");
                idUF       = c.resultado.getInt("idUF");
                nomeCidade = c.resultado.getString("nomeCidade");

                EnderecoCidade cid = new EnderecoCidade(idCidade, nomeCidade, idUF);
                cidades.add(cid);
            }
        
            c.resultado.close();
            
            c.fechaBD();
            
            return cidades;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
}
