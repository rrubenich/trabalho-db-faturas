/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.Cliente;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterCliente {
    Conexao c = new Conexao();
    
    public boolean Create(Cliente cli){
        
          
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO cliente VALUES(null,?,?,?,?,?,?,?,?)");
            c.declaracao.setString(1, cli.getNomeCliente());
            c.declaracao.setString(2, cli.getEmailCliente());
            c.declaracao.setInt(3, cli.getDddCliente());
            c.declaracao.setInt(4, cli.getTelefoneCliente());
            c.declaracao.setString(5, cli.getCpfCliente());
            c.declaracao.setString(6, cli.getEnderecoComplemento());
            c.declaracao.setInt(7, cli.getEnderecoNumero());
            c.declaracao.setInt(8, cli.getIdEndereco());
            
            c.declaracao.execute();

            c.fechaBD();
            return true;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return false;
        }
    }

    public Cliente Read(int idCliente){
        
        try {
            
            String nomeCliente = null;
            String emailCliente= null;
            int dddCliente = 0;
            int telefoneCliente = 0;
            String cpfCliente = null;
            String enderecoComplemento = null;
            int enderecoNumero = 0;
            int idEndereco = 0;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM cliente WHERE idCliente = ?");
            c.declaracao.setInt(1, idCliente);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               nomeCliente          = c.resultado.getString("nomeCliente");
               emailCliente         = c.resultado.getString("emailCliente");
               dddCliente           = c.resultado.getInt("dddCliente");
               telefoneCliente      = c.resultado.getInt("telefoneCliente");
               cpfCliente           = c.resultado.getString("cpfCliente");
               enderecoComplemento  = c.resultado.getString("enderecoComplemento");
               enderecoNumero       = c.resultado.getInt("enderecoNumero");
               idEndereco           = c.resultado.getInt("idEndereco");
            }
            
            Cliente cli = new Cliente(idCliente, nomeCliente, emailCliente, 
                        dddCliente, telefoneCliente, cpfCliente, enderecoComplemento, enderecoNumero, idEndereco);
        
            
            c.fechaBD();
            return cli;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idCliente){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM cliente WHERE idCliente = ?");
            c.declaracao.setInt(1, idCliente);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<Cliente> List(){
        ArrayList<Cliente> clientes = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idCliente;
            String nomeCliente;
            String emailCliente;
            int dddCliente;
            int telefoneCliente;
            String cpfCliente;
            String enderecoComplemento;
            int enderecoNumero;
            int idEndereco;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM cliente");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idCliente            = c.resultado.getInt("idCliente");
               nomeCliente          = c.resultado.getString("nomeCliente");
               emailCliente         = c.resultado.getString("emailCliente");
               dddCliente           = c.resultado.getInt("dddCliente");
               telefoneCliente      = c.resultado.getInt("telefoneCliente");
               cpfCliente           = c.resultado.getString("cpfCliente");
               enderecoComplemento  = c.resultado.getString("enderecoComplemento");
               enderecoNumero       = c.resultado.getInt("enderecoNumero");
               idEndereco           = c.resultado.getInt("idEndereco");
                
                Cliente cli = new Cliente(idCliente, nomeCliente, emailCliente, 
                        dddCliente, telefoneCliente, cpfCliente, enderecoComplemento, enderecoNumero, idEndereco);
        
                clientes.add(cli);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return clientes;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<Cliente> ListNamesBySeller(int idVendedor){
        ArrayList<Cliente> clientes = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idCliente;
            String nomeCliente;
            
            c.declaracao = c.conexao.prepareStatement("SELECT cliente.idCliente, cliente.nomeCliente" +
                    "FROM cliente" +
                    "INNER JOIN notaFiscal" +
                    "ON notaFiscal.idCliente = cliente.idCliente" +
                    "WHERE notaFiscal.idVendedor = ?");
            
            c.declaracao.setInt(1, idVendedor);
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idCliente            = c.resultado.getInt("idCliente");
                nomeCliente          = c.resultado.getString("nomeCliente");
                
                Cliente cli = new Cliente(idCliente, nomeCliente);
                clientes.add(cli);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return clientes;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<Cliente> Search(String query){
        ArrayList<Cliente> clientes = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idCliente;
            String nomeCliente;
            
            
            c.declaracao = c.conexao.prepareStatement("SELECT idCliente, nomeCliente FROM cliente WHERE nomeCliente LIKE ?");
            c.declaracao.setString(1, "%" + query + "%");
            c.resultado = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idCliente   = c.resultado.getInt("idCliente");
                nomeCliente = c.resultado.getString("nomeCliente");
                
                Cliente cli = new Cliente(idCliente, nomeCliente);
                clientes.add(cli);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return clientes;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    
//    public boolean Update(Cliente cli, String cnpj){
//        try{
//            c.abreBD();
//
//            c.declaracao = c.conexao.prepareStatement("UPDATE distribuidor"
//                    + " SET nomeEmpresa = ?,"
//                    + "nomeFantasia = ?,"
//                    + "dddDistribuidor = ?"
//                    + "telefoneDistribuidor = ?"
//                    + "enderecoNumero = ?"
//                    + "enderecoComplemento = ?"
//                    + "WHERE cnpj = ?");
//            
//            c.declaracao.setString(7, d.getCnpj());
//            
//            c.declaracao.setString(1, d.getNomeEmpresa());
//            c.declaracao.setString(2, d.getNomeFantasia());
//            c.declaracao.setInt(3, d.getDddDistribuidor());
//            c.declaracao.setInt(4, d.getTelefoneDistribuidor());
//            c.declaracao.setInt(5, d.getEnderecoNumero());
//            c.declaracao.setString(6, d.getEnderecoComplemento());
//            
//            c.declaracao.execute();
//
//            c.fechaBD();
//            return true;
//        }
//        catch(SQLException ex){
//            System.out.println(ex);
//            return false;
//        }
//    }
    
   
}
