/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.NotaFiscal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterNotaFiscal {
    Conexao c = new Conexao();
    
    public int Create(NotaFiscal not){
        int ret = 0;
        
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO notaFiscal VALUES(null,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            c.declaracao.setDate(1, not.getDataEmissao());
            c.declaracao.setInt(2, not.getIdCliente());
            c.declaracao.setInt(3, not.getIdVendedor());
             c.declaracao.execute();
            
            try (ResultSet keys = c.declaracao.getGeneratedKeys()) {

                keys.next();
                ret = keys.getInt(1);
                keys.close();
            }
            
            c.fechaBD();
        }
        
        catch (SQLException ex){
            System.out.println(ex);
        }
        return ret;
    }

    public NotaFiscal Read(int idNotaFiscal){
        
        try {
            
            
           Date dataEmissao = null;
           int idCliente = 0;
           int idVendedor = 0;
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM notaFiscal WHERE idNotaFiscal = ?");
            c.declaracao.setInt(1, idNotaFiscal);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               dataEmissao = c.resultado.getDate("dataEmissao");
               idCliente   = c.resultado.getInt("idCliente");
               idVendedor  = c.resultado.getInt("idVendedor");
            }
            
            NotaFiscal not = new NotaFiscal(idNotaFiscal, dataEmissao, idCliente, idVendedor);
        
            
            c.fechaBD();
            return not;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idNotaFiscal){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM notaFiscal WHERE idNotaFiscal = ?");
            c.declaracao.setInt(1, idNotaFiscal);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<NotaFiscal> List(){
        ArrayList<NotaFiscal> notas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idNotaFiscal;
            Date dataEmissao;
            int idCliente;
            int idVendedor;
    
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM cliente");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idNotaFiscal = c.resultado.getInt("idNotaFiscal");
               dataEmissao  = c.resultado.getDate("dataEmissao");
               idCliente    = c.resultado.getInt("idCliente");
               idVendedor   = c.resultado.getInt("idVendedor");
               
            
               NotaFiscal not = new NotaFiscal(idNotaFiscal, dataEmissao, idCliente, idVendedor);
        
            
               notas.add(not);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return notas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
}
