/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.Vendedor;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterVendedor {
        
    Conexao c = new Conexao();
    
    public boolean Create(Vendedor ven){
        
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO vendedor VALUES(null,?)");
            c.declaracao.setString(1, ven.getNomeVendedor());
            
            c.declaracao.execute();

            c.fechaBD();
            return true;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return false;
        }
    }

    public Vendedor Read(int idVendedor){
        
        try {
            
            String nomeVendedor = null;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM vendedor WHERE idVendedor = ?");
            c.declaracao.setInt(1, idVendedor);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               nomeVendedor = c.resultado.getString("nomeVendedor");
            }
            
            Vendedor bar = new Vendedor(idVendedor, nomeVendedor);
        
            
            c.fechaBD();
            return bar;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idVendedor){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM vendedor WHERE idVendedor = ?");
            c.declaracao.setInt(1, idVendedor);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<Vendedor> List(){
        ArrayList<Vendedor> vendedores = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idVendedor;
            String nomeVendedor;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM vendedor");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idVendedor   = c.resultado.getInt("idVendedor");
               nomeVendedor = c.resultado.getString("nomeVendedor");
                
                
               Vendedor ven = new Vendedor(idVendedor, nomeVendedor);
        
            
               vendedores.add(ven);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return vendedores;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<Vendedor> Search(String query){
        ArrayList<Vendedor> vendedores = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idVendedor;
            String nomeVendedor;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM vendedor WHERE nomeVendedor LIKE ?");
            c.declaracao.setString(1, "%" + query + "%");
            c.resultado = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idVendedor   = c.resultado.getInt("idVendedor");
               nomeVendedor = c.resultado.getString("nomeVendedor");
                
                
               Vendedor ven = new Vendedor(idVendedor, nomeVendedor);
               vendedores.add(ven);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return vendedores;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
}
