/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.EnderecoBairro;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterBairro {
    
    Conexao c = new Conexao();
    
    public int Create(EnderecoBairro bar){
        int ret = 0;
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO enderecoBairro VALUES(null,?)", Statement.RETURN_GENERATED_KEYS);
            c.declaracao.setString(1, bar.getNomeBairro());
            
            c.declaracao.execute();

            try (ResultSet keys = c.declaracao.getGeneratedKeys()) {

                keys.next();
                ret = keys.getInt(1);
                keys.close();
            }
            
            c.fechaBD();
            
            return ret;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return ret;
        } 
    }

    public EnderecoBairro Read(int idBairro){
        try {
            
            String nomeBairro = null;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoBairro WHERE idBairro = ?");
            c.declaracao.setInt(1, idBairro);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               nomeBairro = c.resultado.getString("nomeBairro");
            }
            
            EnderecoBairro bar = new EnderecoBairro(idBairro, nomeBairro);
        
            
            c.fechaBD();
            return bar;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idBairro){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM enderecoBairro WHERE idBairro = ?");
            c.declaracao.setInt(1, idBairro);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<EnderecoBairro> List(){
        ArrayList<EnderecoBairro> bairros = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idBairro;
            String nomeBairro;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoBairro");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idBairro   = c.resultado.getInt("idBairro");
               nomeBairro = c.resultado.getString("nomeBairro");
                
                
               EnderecoBairro bar = new EnderecoBairro(idBairro, nomeBairro);
        
            
               bairros.add(bar);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return bairros;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<EnderecoBairro> Search(String query){
        ArrayList<EnderecoBairro> bairros = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idBairro;
            String nomeBairro;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoBairro WHERE nomeBairro LIKE ?");
            c.declaracao.setString(1, "%" + query + "%");
            c.resultado = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idBairro   = c.resultado.getInt("idBairro");
                nomeBairro = c.resultado.getString("nomeBairro");

                EnderecoBairro bar = new EnderecoBairro(idBairro, nomeBairro);
                bairros.add(bar);
            }
        
            c.resultado.close();
            
            c.fechaBD();
            
            return bairros;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
}
