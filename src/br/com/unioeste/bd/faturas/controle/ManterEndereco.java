/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.Endereco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterEndereco {
    Conexao c = new Conexao();
    
    public int Create(Endereco end){
        
        int ret = 0;
        try{
            c.abreBD();
            
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM endereco WHERE cep = ? AND idRua = ? AND idBairro = ? AND idCidade = ?");
            c.declaracao.setString(1, end.getCep());
            c.declaracao.setInt(2, end.getIdRua());
            c.declaracao.setInt(3, end.getIdBairro());
            c.declaracao.setInt(4, end.getIdCidade());
            c.resultado = c.declaracao.executeQuery();
 
            while(c.resultado.next()){
               ret = c.resultado.getInt("idEndereco");
            }
            c.fechaBD();
            
            if(ret == 0){
                ret = BasicCreate(end);
            }
            
            return ret;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return ret;
        }
    }
    
    public int BasicCreate(Endereco end){
        
        int ret = 0;
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO endereco VALUES(null,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            c.declaracao.setString(1, end.getCep());
            c.declaracao.setInt(2, end.getIdRua());
            c.declaracao.setInt(3, end.getIdBairro());
            c.declaracao.setInt(4, end.getIdCidade());
            
            c.declaracao.execute();
            
            try (ResultSet keys = c.declaracao.getGeneratedKeys()) {

                keys.next();
                ret = keys.getInt(1);
                keys.close();
            }
            
            c.fechaBD();
            
            return ret;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return ret;
        }
    }

    public Endereco Read(int idEndereco){
        
        try {
            
            String cep = null;
            int idRua = 0;
            int idBairro = 0;
            int idCidade = 0;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM endereco WHERE idEndereco = ?");
            c.declaracao.setInt(1, idEndereco);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               idRua     = c.resultado.getInt("idRua");
               idBairro  = c.resultado.getInt("idBairro");
               idCidade  = c.resultado.getInt("idCidade");
               cep       = c.resultado.getString("cep");
            }
            
            Endereco end = new Endereco(idEndereco, cep, idRua, idBairro, idCidade);
        
            
            c.fechaBD();
            return end;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idEndereco){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM endereco WHERE idEndereco = ?");
            c.declaracao.setInt(1, idEndereco);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<Endereco> List(){
        ArrayList<Endereco> enderecos = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idEndereco;
            String cep;
            int idRua;
            int idBairro;
            int idCidade;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM cliente");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idEndereco = c.resultado.getInt("idEndereco");
               cep        = c.resultado.getString("cep");
               idRua      = c.resultado.getInt("idRua");
               idBairro   = c.resultado.getInt("idBairro");
               idCidade   = c.resultado.getInt("idCidade");
                
                
               Endereco end = new Endereco(idEndereco, cep, idRua, idBairro, idCidade);
        
            
               enderecos.add(end);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return enderecos;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
}
