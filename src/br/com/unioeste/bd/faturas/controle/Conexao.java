package br.com.unioeste.bd.faturas.controle;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.sql.Connection; 
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rafakx
 */
public class Conexao {
    public Connection conexao;
    public PreparedStatement declaracao;
    public ResultSet resultado;
    
    
    public void abreBD(){
        try{
            Class.forName("com.mysql.jdbc.Driver"); 
        
            String url = "jdbc:mysql://127.0.0.1:3306/Faturas";
            String user = "root";
            String password = "fuDcwCN5TAvVDxaW";

            conexao = DriverManager.getConnection(url, user, password);
        }
        catch(ClassNotFoundException | SQLException ex)
        {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public void fechaBD(){
        try {
            conexao.close();
        }
        catch(SQLException ex)
        {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}