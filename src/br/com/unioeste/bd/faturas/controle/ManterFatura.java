/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.Fatura;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterFatura {
    Conexao c = new Conexao();
    
    public boolean Create(Fatura fat){
        
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO fatura VALUES(null,?,?,?,?,?)");
            c.declaracao.setDate(1, fat.getDataEmissao());
            c.declaracao.setDate(2, fat.getDataVencimento());
            c.declaracao.setInt(3, fat.getValorFatura());
            c.declaracao.setInt(4, fat.getIdSituacao());
            c.declaracao.setInt(5, fat.getIdNotaFiscal());
            
            c.declaracao.execute();

            c.fechaBD();
            return true;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return false;
        }
    }
    
    public ArrayList<Fatura> List(){
        ArrayList<Fatura> faturas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idFatura;
            Date dataEmissao;
            Date dataVencimento;
            int valorFatura;
            int idSituacao;
            int idNotaFiscal;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM fatura");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idFatura        = c.resultado.getInt("idFatura");
                dataEmissao     = c.resultado.getDate("dataEmissao");
                dataVencimento  = c.resultado.getDate("dataEmissao");
                valorFatura     = c.resultado.getInt("valorFatura");
                idSituacao      = c.resultado.getInt("idSituacao");
                idNotaFiscal    = c.resultado.getInt("idNotaFiscal");
                
                
                Fatura fat = new Fatura(idFatura, dataEmissao, dataVencimento, 
                        valorFatura, idSituacao, idNotaFiscal);
                faturas.add(fat);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return faturas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<Fatura> ListByCity(int idCidade){
        ArrayList<Fatura> faturas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idFatura;
            Date dataEmissao;
            Date dataVencimento;
            int valorFatura;
            int idSituacao;
            int idNotaFiscal;
            
            
            c.declaracao = c.conexao.prepareStatement("SELECT fatura.*" +
                    "FROM fatura" +
                    "INNER JOIN notaFiscal" +
                    "ON fatura.idNotaFiscal = notaFiscal.idNotaFiscal" +
                    "INNER JOIN cliente" +
                    "ON notaFiscal.idCliente = cliente.idCliente" +
                    "INNER JOIN endereco" +
                    "ON cliente.idEndereco = endereco.idEndereco" +
                    "WHERE endereco.idCidade = ?");
            
            c.declaracao.setInt(1, idCidade);
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idFatura        = c.resultado.getInt("idFatura");
                dataEmissao     = c.resultado.getDate("dataEmissao");
                dataVencimento  = c.resultado.getDate("dataEmissao");
                valorFatura     = c.resultado.getInt("valorFatura");
                idSituacao      = c.resultado.getInt("idSituacao");
                idNotaFiscal    = c.resultado.getInt("idNotaFiscal");
                
                
                Fatura fat = new Fatura(idFatura, dataEmissao, dataVencimento, 
                        valorFatura, idSituacao, idNotaFiscal);
                faturas.add(fat);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return faturas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<Fatura> ListByDistrict(int idCidade, int idBairro){
        ArrayList<Fatura> faturas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idFatura;
            Date dataEmissao;
            Date dataVencimento;
            int valorFatura;
            int idSituacao;
            int idNotaFiscal;
            
            
            c.declaracao = c.conexao.prepareStatement("SELECT fatura.*" +
                    "FROM fatura" +
                    "INNER JOIN notaFiscal" +
                    "ON fatura.idNotaFiscal = notaFiscal.idNotaFiscal" +
                    "INNER JOIN cliente" +
                    "ON notaFiscal.idCliente = cliente.idCliente" +
                    "INNER JOIN endereco" +
                    "ON cliente.idEndereco = endereco.idEndereco" +
                    "WHERE endereco.idCidade = ?" +
                    "AND endereco.idBairro = ?");
            
            c.declaracao.setInt(1, idCidade);
            c.declaracao.setInt(2, idBairro);
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idFatura        = c.resultado.getInt("idFatura");
                dataEmissao     = c.resultado.getDate("dataEmissao");
                dataVencimento  = c.resultado.getDate("dataEmissao");
                valorFatura     = c.resultado.getInt("valorFatura");
                idSituacao      = c.resultado.getInt("idSituacao");
                idNotaFiscal    = c.resultado.getInt("idNotaFiscal");
                
                
                Fatura fat = new Fatura(idFatura, dataEmissao, dataVencimento, 
                        valorFatura, idSituacao, idNotaFiscal);
                faturas.add(fat);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return faturas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<Fatura> ListBySeller(int idVendedor){
        ArrayList<Fatura> faturas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idFatura;
            Date dataEmissao;
            Date dataVencimento;
            int valorFatura;
            int idSituacao;
            int idNotaFiscal;
            
            
            c.declaracao = c.conexao.prepareStatement("SELECT fatura.*" +
                    "FROM fatura" +
                    "INNER JOIN notaFiscal" +
                    "ON fatura.idNotaFiscal = notaFiscal.idNotaFiscal" +
                    "INNER JOIN vendedor" +
                    "ON notaFiscal.idVendedor = vendedor.idVendedor" +
                    "WHERE vendedor.idVendedor = ?");
            
            c.declaracao.setInt(1, idVendedor);
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idFatura        = c.resultado.getInt("idFatura");
                dataEmissao     = c.resultado.getDate("dataEmissao");
                dataVencimento  = c.resultado.getDate("dataEmissao");
                valorFatura     = c.resultado.getInt("valorFatura");
                idSituacao      = c.resultado.getInt("idSituacao");
                idNotaFiscal    = c.resultado.getInt("idNotaFiscal");
                
                
                Fatura fat = new Fatura(idFatura, dataEmissao, dataVencimento, 
                        valorFatura, idSituacao, idNotaFiscal);
                
                faturas.add(fat);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return faturas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    public ArrayList<Fatura> ListByReceipt(int idNotaFiscal){
        ArrayList<Fatura> faturas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idFatura;
            Date dataEmissao;
            Date dataVencimento;
            int valorFatura;
            int idSituacao;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM fatura WHERE fatura.idNotaFiscal = ?");
            
            c.declaracao.setInt(1, idNotaFiscal);
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idFatura        = c.resultado.getInt("idFatura");
                dataEmissao     = c.resultado.getDate("dataEmissao");
                dataVencimento  = c.resultado.getDate("dataEmissao");
                valorFatura     = c.resultado.getInt("valorFatura");
                idSituacao      = c.resultado.getInt("idSituacao");
                idNotaFiscal    = c.resultado.getInt("idNotaFiscal");
                
                
                Fatura fat = new Fatura(idFatura, dataEmissao, dataVencimento, 
                        valorFatura, idSituacao, idNotaFiscal);
                
                faturas.add(fat);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return faturas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
}
