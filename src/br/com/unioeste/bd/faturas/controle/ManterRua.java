/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unioeste.bd.faturas.controle;

import br.com.unioeste.bd.faturas.modelo.EnderecoRua;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author rafakx
 */
public class ManterRua {

    Conexao c = new Conexao();
    
    public int Create(EnderecoRua rua){
        int ret = 0;
        try{
            c.abreBD();

            c.declaracao = c.conexao.prepareStatement("INSERT INTO enderecoRua VALUES(null,?)", Statement.RETURN_GENERATED_KEYS);
            c.declaracao.setString(1, rua.getNomeRua());
            
            c.declaracao.execute();

            try (ResultSet keys = c.declaracao.getGeneratedKeys()) {

                keys.next();
                ret = keys.getInt(1);
                keys.close();
            }
            
            c.fechaBD();
            
            return ret;
        }
        catch (SQLException ex){
            System.out.println(ex);
            return ret;
        } 
    }

    public EnderecoRua Read(int idRua){
        
        try {
            
            String nomeRua = null;
    
    
            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoRua WHERE idRua = ?");
            c.declaracao.setInt(1, idRua);
            c.resultado = c.declaracao.executeQuery();
 
            
            while(c.resultado.next()){
               nomeRua = c.resultado.getString("nomeRua");
            }
            
            EnderecoRua rua = new EnderecoRua(idRua, nomeRua);
        
            
            c.fechaBD();
            return rua;

        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }
    
    public boolean Delete(int idRua){
        try{

            c.abreBD();
            c.declaracao = c.conexao.prepareStatement("DELETE FROM enderecoRua WHERE idRua = ?");
            c.declaracao.setInt(1, idRua);
            c.declaracao.execute();
            c.fechaBD();
            
            return true;

        }
        catch(SQLException ex){
            System.out.println(ex);
            return false;
        } 
    }
    
    public ArrayList<EnderecoRua> List(){
        ArrayList<EnderecoRua> ruas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idRua;
            String nomeRua;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoRua");
            c.resultado  = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
               idRua   = c.resultado.getInt("idRua");
               nomeRua = c.resultado.getString("nomeRua");
                
                
               EnderecoRua cid = new EnderecoRua(idRua, nomeRua);
        
            
               ruas.add(cid);
            }
        
            c.resultado.close();
            c.fechaBD();
            
            return ruas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
    
    public ArrayList<EnderecoRua> Search(String query){
        ArrayList<EnderecoRua> ruas = new ArrayList<>();
        
        try{
            c.abreBD();
            
            int idRua;
            String nomeRua;
            
            c.declaracao = c.conexao.prepareStatement("SELECT * FROM enderecoRua WHERE nomeRua LIKE ?");
            c.declaracao.setString(1, "%" + query + "%");
            c.resultado = c.declaracao.executeQuery();
            
            while(c.resultado.next()){
                idRua   = c.resultado.getInt("idRua");
                nomeRua = c.resultado.getString("nomeRua");

                EnderecoRua cid = new EnderecoRua(idRua, nomeRua);
                ruas.add(cid);
            }
        
            c.resultado.close();
            
            c.fechaBD();
            
            return ruas;
            
        }
        catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }
    
}
